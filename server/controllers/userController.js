const User = require('../models/user');
const crypto = require('crypto');
const algorithm = 'aes-256-cbc';
const key = crypto.createHash('sha256').update(String(process.env.ENCRYPTION_SECRET)).digest('base64').substr(0, 32);
const iv = crypto.randomBytes(16);

function encrypt(text) {
 let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(key), iv);
 let encrypted = cipher.update(text);
 encrypted = Buffer.concat([encrypted, cipher.final()]);
 return { iv: iv.toString('hex'), encryptedData: encrypted.toString('hex') };
}

function decrypt(text) {
 let iv = Buffer.from(text.iv, 'hex');
 let encryptedText = Buffer.from(text.encryptedData, 'hex');
 let decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(key), iv);
 let decrypted = decipher.update(encryptedText);
 decrypted = Buffer.concat([decrypted, decipher.final()]);
 return decrypted.toString();
}


module.exports.createUser = (req, res) => {
   const { firstName, lastName, phone, fullAddress, ssn } = req.body;
   const newUser = User({ firstName, lastName ,phone,fullAddress, ssn: encrypt(ssn)});
        newUser.save((err, result) => {
          if(err){
            return res.status(400).json({
              error: err
            })
          }
          res.status(200).json({
            message: "Successfully submitted"
          })
        })
}


module.exports.allUsers = (req, res) => {
   User.find().exec((err, result) => {
     if(err){
       return res.status(400).json({
         error: err
       })
     }
       const decryptedData = result.map((item) => {
       const {firstName, lastName, phone, fullAddress, ssn} = item;
       const data = {firstName, lastName, phone, fullAddress, ssn: decrypt(ssn)}
        return data;
     })

     return res.status(200).json({
       result: decryptedData
     })
   })
}
